# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields
from trytond.transaction import Transaction
from trytond.pyson import If, In, Eval


class AnalyticConcept(ModelSQL, ModelView):
    '''Analytic account concept'''
    __name__ = 'analytic_account.concept'
    _order_name = 'code'

    company = fields.Many2One('company.company', 'Company', required=True,
        domain=[
            ('id', If(In('company', Eval('context', {})), '=', '!='),
                Eval('context', {}).get('company', -1))])
    code = fields.Char('Code')
    name = fields.Char('Name', required=True)
    lines = fields.One2Many('analytic_account.concept.line',
        'concept', 'Lines',
        order=[('account.code', 'ASC')])

    @staticmethod
    def default_company():
        return Transaction().context.get('company', None)

    def get_rec_name(self, name):
        if self.code:
            return '%s %s' % (self.code, self.name)
        return self.name


class AnalyticConceptLine(ModelSQL, ModelView):
    '''Analytic account concept line'''
    __name__ = 'analytic_account.concept.line'

    concept = fields.Many2One('analytic_account.concept', 'Concept',
        required=True, ondelete='CASCADE', select=True)
    company = fields.Function(fields.Many2One('company.company', 'Company'),
        'on_change_with_company')
    account = fields.Many2One('account.account', 'Account',
        ondelete='RESTRICT', required=True, domain=[
            ('company', '=', Eval('company')),
            ('kind', '!=', 'view')],
        depends=['company'])
    analytic_account = fields.Many2One('analytic_account.account',
        'Analytic Account',
        ondelete='RESTRICT', domain=[
            ('company', '=', Eval('company')),
            ('type', 'in', ['normal', 'distribution'])],
        depends=['company'])

    @fields.depends('concept', '_parent_concept.company')
    def on_change_with_company(self, name=None):
        if self.concept:
            return self.concept.company.id
        return None
