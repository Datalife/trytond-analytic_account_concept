=================================
Analytic Account Concept Scenario
=================================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules

Install analytic_account_concept::

    >>> config = activate_modules('analytic_account_concept')
