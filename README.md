datalife_analytic_account_concept
=================================

The analytic_account_concept module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-analytic_account_concept/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-analytic_account_concept)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
